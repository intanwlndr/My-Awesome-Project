//Chat Box
var awal = "msg-receive";
function enterFunc(e) {
    var x = document.getElementById("enter-msg").value;
    
    if(e.keyCode == 13) {
        if (x!="") {
            var node = document.createElement("DIV");
            if (awal == "msg-receive") {
                awal = "msg-opening";
            }
            else {
                awal = "msg-receive";
            }
            node.className = awal;
            var textnode = document.createTextNode(x);
            node.appendChild(textnode);
            document.getElementById("in-msg").appendChild(node);
            document.getElementById("enter-msg").value = "";
            e.preventDefault();
        }
    }

}
$('.chat-head').click(function(){
  $('.chat-body').slideToggle();
})


localStorage.setItem("themes", JSON.stringify([
  {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
  {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
  {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
  {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
  {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
  {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
  {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
  {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
  {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
  {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
  {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]));
localStorage.setItem("selectedTheme", JSON.stringify({"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}));

//Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    erase = false;
    print.value = null;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
//END

//select2

$(document).ready(function () {
  $('.my-select').select2({
      data: JSON.parse(localStorage.getItem("themes"))
  });


  $('.apply-button').on('click', function () {  // sesuaikan class button
      theme = JSON.parse(localStorage.getItem('themes'))[$('.my-select').val()];
      $('body').css({"backgroundColor": theme['bcgColor']});
      $('.text-center').css({"color": theme['fontColor']});
      localStorage.setItem('selectedTheme', JSON.stringify(theme));
  });


});