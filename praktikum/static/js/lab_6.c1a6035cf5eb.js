//Chat Box
$('.chat-head').click(function(){
  $('.chat-body').slideToggle();
})
//END

//Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    erase = false;
    print.value = "0";
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
//END

//select2
localStorage.setItem("themes", JSON.stringify([
  {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
  {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
  {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
  {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
  {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
  {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
  {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
  {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
  {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
  {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
  {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
]));

localStorage.setItem("selectedTheme", JSON.stringify({"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}));

$('.my-select').select2(
  {'data' : themes}).val(theme['id']).change();
$(document).ready(function() {
  $('.apply-button').on('click', function(){
      theme = themes[$('.my-select').val()];
      changeTheme(theme);
      localStorage.setItem('selectedTheme',JSON.stringify(theme));
  })
});
