from django.conf.urls import url
from .views import index, add_todo

#url for app, add your URL Configuration

urlpatterns = [
      url(r'^$', index , name='index'),
      url(r'^add_todo', add_todo, name='add_todo')
]
